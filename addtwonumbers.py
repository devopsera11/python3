"""
Given two non-empty lists representing two non-negative integers.
Each element of the list,contains a single digit.
Add the two numbers and return the sum as a list.
You may assume the two numbers do not contain any leading zero,
except the number 0 itself.
Example:
input:
l1 = [1, 2, 3]
l2 = [9, 9, 9]
output:
[0, 2, 3]
"""

class Calc:
    def __init__(self, l1, l2):
        self.l1 = l1
        self.l2 = l2
        self.remainder = 0
        self.result = []

    def calcu(self):
        for i in range(len(self.l1)):
            total = self.l1[i] + self.l2[i] + self.remainder
            if total >= 10:
                self.remainder = 1
                num = total - 10
                self.result.append(num)
            else:
                self.remainder = 0
                self.result.append(total)
        return self.result


calculator = Calc([1, 2, 3], [9, 9, 9])
print(calculator.calcu())
