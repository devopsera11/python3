"""

Given an integer array nums,
return true if any value appears at least twice in the array,
and return false if every element is distinct.

"""

class Duplicate:
    def containsDuplicate(self, nums):
        self.nums = nums
        list_length = len(self.nums)
        set_length = len(set(self.nums))
        if list_length == set_length:
            return False
        else:
            return True



"""

LESS optimal solution below, which adds more complexity:

class Solution():
	def containsDuplicate(self, nums):
		self.nums = nums
		flag = False
		for i in range(len(self.nums)):
			if self.nums.count(i) >= 2:
				flag = True
				break
		return flag

"""
