"""
Given an integer x, return true if x is a 
palindrome, and false otherwise.

Example 1:

Input: x = 121
Output: true
Explanation: 121 reads as 121 from left to right and from right to left.

"""

class PalindromeNum:
    def isPalindrome(self, x):
        self.x = x
        flag = False
        if self.x >= 0:
            result = []
            for char in str(self.x):
                if char.isdigit():
                    result.append(int(char))
            length = len(result) - 1
            if len(result) == 1:
                flag = True
            elif len(result) > 1:
                flag = True
                for i in range(int(len(result) / 2)):
                    if result[i] != result[length]:
                        flag = False
                        break
                    length -= 1
        return flag