"""

Find the Index of the First Occurrence in a String

Given two strings needle and haystack, 
return the index of the first occurrence of needle 
in haystack, or -1 if needle is not part of haystack.

"""

class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        h_index = haystack.find(needle)
        if h_index:
            return h_index
        else:
            return 0
