"""

Add Binary

Given two binary strings a and b, 
return their sum as a binary string.



"""

class Solution:
    def addBinary(self, a: str, b: str) -> str:
        int_a = int(a,2)
        int_b = int(b,2)
        int_sum = int_a + int_b
        binary_sum = bin(int_sum)[2:]
        if not binary_sum[0]:
            binary_sum = binary_sum[1:len(binary_sum)-1:1]
        return binary_sum