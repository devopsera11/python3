"""

Given two strings s and t,
return true if t is an anagram of s,
and false otherwise.

An Anagram is a word or phrase formed by 
rearranging the letters of a different word or phrase,
 typically using all the original letters exactly once.

"""


class Solution:
    def isAnagram(self, s: str, t: str):
        self.s = s
        self.t = t
        sorted_s = sorted(self.s)
        sorted_t = sorted(self.t)
        assert sorted_s == list, "non valid type"  #statement to compare sorted_s with the list class
        assert sorted_t == list, "non valid type"
        return sorted_s == sorted_t
