class Solution:
    def longestCommonPrefix(self, str1: str, str2: str) -> str:
        len1 = len(str1)
        len2 = len(str2)
        str3 = ""
        if len1 >= len2:
            for i in range(min(len1, len2)):
                if str1[i] == str2[i]:
                    str3 += str2[i]
                else:
                    break
        else:
            for j in range(min(len1, len2)):
                if str1[j] == str2[j]:
                    str3 += str1[j]
                else:
                    break
        return str3
