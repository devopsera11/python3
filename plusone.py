"""

You are given a large integer represented 
as an integer array digits, 
where each digits[i] is the ith digit of the integer. 
The digits are ordered from most significant
 to least significant in left-to-right order. 
 The large integer does not contain any leading 0's.

Increment the large integer by one and 
return the resulting array of digits.

"""

class Solution:
    def plusOne(self, digits: list[int]) -> list[int]:
        increment = digits[len(digits)-1] + 1
        if increment > 9:
            digits[len(digits)-1] = 1
            digits.append(increment - 10)
        else:
            digits[len(digits)-1] = increment
        return digits



"""
Other solution:

class Solution:
    def plusOne(self, digits: list[int]) -> list[int]:
        carry = 1  # Initialize the carry to 1
        for i in range(len(digits) - 1, -1, -1):  # Iterate through the digits in reverse order
            digits[i] += carry
            carry = digits[i] // 10
            digits[i] %= 10
            if carry == 0:
                break
        if carry == 1:
            digits.insert(0, 1)
        return digits

"""

        