"""

Given a string s consisting of words and spaces, 
return the length of the last word in the string.

A word is a maximal 
substring
 consisting of non-space characters only.

"""

class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        li = list(s)
        assert isinstance(li, list), "Invalid input: 'li' is not a list"

        s = s.split()
        li = list(s[len(s)-1])
        return len(li)
